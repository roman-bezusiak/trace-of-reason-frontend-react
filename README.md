# Trace of Reason (frontend)

![TOR logo](./public/logo1000.png)

## 📋 Description

This project is aimed to provide tooling for data visualization and linkage.
It is available [online](https://trace-of-reason-frontend-react.herokuapp.com/)

## 📥 Installation instructions

1. Clone the repo (`git clone https://gitlab.com/roman-bezusiak/trace-of-reason-frontend-react.git`)
2. Unzip
3. Move into the root directory (`cd trace-of-reason-frontend-react`)

## 🛠 Configuration instructions

1. Install and configure [backend](https://gitlab.com/roman-bezusiak/trace-of-reason-backend-spring) locally
2. Create `.env` file in the root directory
3. In `.env` change `LOCAL_DEPLOYMENT` variable to `true`

## ⚙️ Operating instructions

1. Run backend
2. Run `yarn install`
3. Run `yarn run start`
4. If the browser tab does not open automatically, go to `localhost:3000`
