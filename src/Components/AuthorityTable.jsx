import React, { useState, useEffect } from "react";
import ReactTable from "react-table-v6";
import "react-table-v6/react-table.css";
import APIConfig from '../Config/APIConfig';

const AuthorityTable = props => {
  const [authorities, setAuthorities] = useState([]);
  
  let req = new Request(
    APIConfig.requestURLs.authority.getAll,
    {
      method: 'GET',
      headers: APIConfig.requestHeaders
    }
  );

  useEffect(() => {
    fetch(req)
      .then(res => {
        if (!res.ok) throw Error(`Response.status = ${res.status}`);
        else res.json().then(json => {
          if (json !== null) setAuthorities(json);
          else throw Error('Response.json() = null');
        });
      })
      .catch(err => console.error(`AuthorityTable.jsx:\n  ${err}`));
  }, []);

  const columns = [
    APIConfig.ReactTableColumns.id,
    APIConfig.ReactTableColumns.name,
    APIConfig.ReactTableColumns.url,
    APIConfig.ReactTableColumns.creationTimestampTZ,
    APIConfig.ReactTableColumns.updateTimestampTZ
  ];

  return (
    <div className="AuthorityTable" style={{gridArea: "a"}}>
      <h4>{props.title}</h4>
      <ReactTable
        data={authorities}
        columns={columns}
        showPagination={false}
        defaultPageSize={6}
        showPaginationTop
      />
    </div>
  );
};

export default AuthorityTable;
