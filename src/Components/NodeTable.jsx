import React, { useState, useEffect } from "react";
import ReactTable from "react-table-v6";
import "react-table-v6/react-table.css";
import APIConfig from '../Config/APIConfig';

const NodeTable = props => {
  const [nodes, setNodes] = useState([]);

  let req = new Request(
    APIConfig.requestURLs.node.getAll,
    {
      method: 'GET',
      headers: APIConfig.requestHeaders
    }
  );

  useEffect(() => {
    fetch(req)
      .then(res => {
        if (!res.ok) throw Error(`Response.status = ${res.status}`);
        else res.json().then(json => {
          if (json !== null) setNodes(json);
          else throw Error('Response.json() = null');
        });
      })
      .catch(err => console.error(`NodeTable.jsx:\n  ${err}`));
  }, []);

  const columns = [
    APIConfig.ReactTableColumns.id,
    APIConfig.ReactTableColumns.name,
    APIConfig.ReactTableColumns.url,
    APIConfig.ReactTableColumns.creationTimestampTZ,
    APIConfig.ReactTableColumns.updateTimestampTZ,
    APIConfig.ReactTableColumns.aliases,
    APIConfig.ReactTableColumns.authorityIds,
    APIConfig.ReactTableColumns.groupIds,
    APIConfig.ReactTableColumns.parentIds,
    APIConfig.ReactTableColumns.childIds
  ];

  return (
    <div className="NodeTable" style={{gridArea: "n"}}>
      <h4>{props.title}</h4>
      <ReactTable
        data={nodes}
        columns={columns}
        showPagination={false}
        defaultPageSize={6}
        showPaginationTop
      />
    </div>
  );
};

export default NodeTable;
