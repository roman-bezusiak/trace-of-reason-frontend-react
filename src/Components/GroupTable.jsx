import React, { useState, useEffect } from 'react';
import ReactTable from 'react-table-v6';
import 'react-table-v6/react-table.css';
import APIConfig from '../Config/APIConfig';

const GroupTable = props => {
  const [groups, setGroups] = useState([]);

  let req = new Request(
    APIConfig.requestURLs.group.getAll,
    {
      method: 'GET',
      headers: APIConfig.requestHeaders
    }
  );

  useEffect(() => {
    fetch(req)
      .then(res => {
        if (!res.ok) throw Error(`Response.status = ${res.status}`);
        else res.json().then(json => {
          if (json !== null) setGroups(json);
          else throw Error('Response.json() = null');
        });
      })
      .catch(err => console.error(`GroupTable.jsx:\n  ${err}`));
  }, []);

  const columns = [
    APIConfig.ReactTableColumns.id,
    APIConfig.ReactTableColumns.name,
    APIConfig.ReactTableColumns.description,
    APIConfig.ReactTableColumns.creationTimestampTZ,
    APIConfig.ReactTableColumns.updateTimestampTZ,
    APIConfig.ReactTableColumns.aliases,
    APIConfig.ReactTableColumns.authorityIds
  ];

  return (
    <div className='GroupTable' style={{gridArea: 'g'}}>
      <h4>{props.title}</h4>
      <ReactTable
        data={groups}
        columns={columns}
        showPagination={false}
        defaultPageSize={6}
        showPaginationTop
      />
    </div>
  );
};

export default GroupTable;
