import React from 'react';
import { render } from '@testing-library/react';
import App from '../App/App';

test('renders header', () => {
  const { getByText } = render(<App />);
  
  const nodeTableHeaderElement = getByText(/Node table/i);
  const groupTableHeaderElement = getByText(/Group table/i);
  const authorityTableHeaderElement = getByText(/Authority table/i);
  
  expect(nodeTableHeaderElement).toBeInTheDocument();
  expect(groupTableHeaderElement).toBeInTheDocument();
  expect(authorityTableHeaderElement).toBeInTheDocument();
});
