let requestHeaders = new Headers();

requestHeaders.append('Accept', 'application/json');
requestHeaders.append('Authorization', 'Basic ' + window.btoa('admin:test'));
requestHeaders.append('Access-Control-Allow-Origin', '*');

const localDeployment = process.env.LOCAL_DEPLOYMENT === 'true';
const localURL = 'http://localhost:5000/';
const herokuURL = 'https://trace-of-reason-backend-spring.herokuapp.com/';
const url = localDeployment ? localURL : herokuURL;

const APIConfig = {
  requestURLs: {
    node: {
      getAll: url + "api/nodes",
    },
    group: {
      getAll: url + "api/groups",
    },
    authority: {
      getAll: url + "api/authorities",
    }
  },
  requestHeaders: requestHeaders,
  ReactTableColumns: {
    id: {
      Header: "Id",
      accessor: "id"
    },
    name: {
      Header: "Name",
      accessor: "name"
    },
    url: {
      Header: "URL",
      accessor: "url"
    },
    description: {
      Header: "Description",
      accessor: "description"
    },
    creationTimestampTZ: {
      Header: "Creation timestamp with timezone",
      accessor: "creationTimestampTZ"
    },
    updateTimestampTZ: {
      Header: "Update timestamp with timezone",
      accessor: "updateTimestampTZ"
    },
    aliases: {
      Header: "Aliases",
      accessor: "aliases"
    },
    authorityIds: {
      Header: "Authority IDs",
      accessor: "authorityIds"
    },
    groupIds: {
      Header: "Group IDs",
      accessor: "groupIds"
    },
    parentIds: {
      Header: "Parent node IDs",
      accessor: "parentIds"
    },
    childIds: {
      Header: "Child node IDs",
      accessor: "childIds"
    }
  }
}

export default APIConfig;