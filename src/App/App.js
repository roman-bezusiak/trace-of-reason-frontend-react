import React from "react";
import "./App.css";
import NodeTable from "../Components/NodeTable";
import GroupTable from "../Components/GroupTable";
import AuthorityTable from "../Components/AuthorityTable";

function App() {
  return (
    <div className="App">
      <NodeTable title="Node table" />
      <GroupTable title="Group table" />
      <AuthorityTable title="Authority table" />
    </div>
  );
}

export default App;
